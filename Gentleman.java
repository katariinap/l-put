package loputoo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Gentleman {

	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("--- TERE TULEMAST KAARDIM�NGU GENTLEMAN ---");
		System.out.println("-- M�NGU EELSEADISTAMISE FAAS --");
		System.out.println("Palun sisesta mitu m�ngijat on:");
		int players = scanner.nextInt();

		ArrayList<Player> playersList = new ArrayList<>(players);

		Deck deck = new Deck(true);
		deck.shuffle();

		for (int p = 0; p < players; p++) {
			Player player = new Player("M�ngija " + p);
			playersList.add(player);
		}
		System.out.println("-- M�NG ALGAB --");
		playGame(playersList);
	}

	public static void playGame(ArrayList<Player> listOfPlayers) {
		int playableCards = 7;
		boolean goingDown = true;

		for (int i = 0; i < 14; i++) {
			// valmistume m�nguks.
			dealCards(listOfPlayers, playableCards);  //jagab k�ikidele kaardid
			askUserBet(listOfPlayers.get(0)); //n�itab kasutajale kaarte ja k�sib panust
			generateBotBets(listOfPlayers); //genereerib bottide panused
			
			for (int j = 0; j < playableCards; j++) {
				Table table = new Table();
				
				// M�ng algab
				Card playedCard = askUserCard(listOfPlayers.get(0)); //k�sib mis kaardi kasutaja lauale paneb
				table.playCard(listOfPlayers.get(0), playedCard); //asetab m�ngija kaardi lauale
				playBotCards(listOfPlayers, table); //asetab bottide kaardid lauale
				Player winner = table.findWinner();	//otsustab, kes v�itis		
				winner.wins++; //saab �he v�idu kirja
				table.printTable(); //prindib v�lja, mida lauale k�idi
				System.out.println("- V�ITJA ON SELGUNUD -");
				System.out.println("V�itis m�ngija nimega " + winner.name);
			}
			
			// Round l�bi, muudame m�ngitavate kaartide arvu			
			if(goingDown) {
				playableCards--;
				if(playableCards == 0) {
					playableCards = 1;
					goingDown = false;
				}
				
			} else {
				playableCards++;
			}
			
			handOutPoints(listOfPlayers); //jagab vastavalt v�itudele punkte, nullib v�itude arvu
			printOutScores(listOfPlayers);
		}
		
		Player winner = findWinner(listOfPlayers); //leiab k�ige suurema skooriga m�ngija
		System.out.println("- M�NG ON L�BI -");
		System.out.println("Kogu m�ngu v�itis: " + winner.name);
	}
	
	public static Player findWinner(ArrayList<Player> listOfPlayers) {
		// teeb koopia (klooni), et mitte algset arraylisti muuta
		ArrayList<Player> temp = (ArrayList<Player>) listOfPlayers.clone(); 
		// sorteerib arraylisti temp m�ngija punktide arvu j�rgi kahanevalt
		Collections.sort(temp, new Comparator<Player>(){
		     public int compare(Player o1, Player o2){
		         if(o1.points == o2.points) return 0;
		         return o1.points < o2.points ? 1 : -1;
		     }
		});
		
		//kohal 0 m�ngija, kellel k�ige rohkem punkte
		return temp.get(0);
	}
	
	public static void printOutScores(ArrayList<Player> listOfPlayers) { //uuesti koopia
		System.out.println("- SKOORID ON J�RGMISED -");
		ArrayList<Player> temp = (ArrayList<Player>) listOfPlayers.clone();
		Collections.sort(temp, new Comparator<Player>(){
		     public int compare(Player o1, Player o2){
		         if(o1.points == o2.points) return 0;
		         return o1.points < o2.points ? 1 : -1;
		     }
		});
		
		for (Player player : temp) {
			System.out.println(player.name + ": " + player.points);
		}
	}
	
	//jagab vastavalt pakkumistele v�lja punkte
	public static void handOutPoints(ArrayList<Player> listOfPlayers) {
		for (Player player : listOfPlayers) {
			if (player.currentBet == player.wins) {
				if (player.currentBet == 0) {
					player.points = player.points + 5;
				} else {
					player.points = player.points + player.wins * 10;
				}
			} else if (player.currentBet > 0) {
				player.points = player.points + player.wins;
			}
			player.wins = 0;
		} 
	}
	
	public static void playBotCards(ArrayList<Player> listOfPlayers, Table table) {
		for (int i = 1; i < listOfPlayers.size(); i++) {
			Card firstCard = listOfPlayers.get(i).hand.getCards().get(0);
			table.playCard(listOfPlayers.get(i), firstCard);
		}
		
	}

	public static void dealCards(ArrayList<Player> listOfPlayers, int numberOfCards) {
		System.out.println("- TOIMUB KAARTIDE JAGAMINE -");
		Deck deck = new Deck();
		deck.shuffle();
		for (Player player : listOfPlayers) {
			Hand hand = new Hand();
			for (int i = 0; i < numberOfCards; i++) {
				hand.addCard(deck.dealCard()); //v�tab deckist �he kaardi ja lisab k�tte
			}
			player.hand = hand; 
		}

	}

	public static void askUserBet(Player player) {
		System.out.println("- KAARDID ON JAGATUD -");
		System.out.print("Sinu kaardid on: ");
		System.out.println(player.hand);
		System.out.println("Palun tee oma pakkumine (mitu k�tt v�idad): ");
		int panus = scanner.nextInt();
		player.currentBet = panus;
		System.out.println("Sa pakkusid " + panus);
	}

	public static void generateBotBets(ArrayList<Player> listOfPlayers) {
		for (int i = 1; i < listOfPlayers.size(); i++) {
			Player player = listOfPlayers.get(i);
			Hand hand = player.hand;
			int bet = 0;
			ArrayList<Card> cards = hand.getCards();

			for (Card card : cards) {
				if (card.getSuit() == Card.HEARTS) {
					bet++;
				}
				if (card.getSuit() == Card.JOKER) {
					bet++;
				}
			}

			player.currentBet = bet;
		}
	}
	
	public static Card askUserCard(Player player) {
		System.out.println("- KAARDI K�IMISE FAAS -");
		int a = 0;
		for (Card card : player.hand.getCards()) {
			System.out.println(a + ": " + card);
			a++;			
		}
		System.out.println("Palun sisesta kaardi number, mida soovid k�ia: ");
		int chosenCardNumber = scanner.nextInt();
		Card chosenCard = player.hand.getCards().get(chosenCardNumber);
		
		return chosenCard;
	}
	
}
