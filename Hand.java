package loputoo;

import java.util.ArrayList;

public class Hand {

	private ArrayList<Card> hand; // Kaardid k�es
	// K�si, mis on esialgu t�hi

	public Hand() {
		hand = new ArrayList<Card>();
	}

	public void clear() { // V�tab k�ik kaardid k�est
		hand.clear();
	}

	public void addCard(Card c) { // Lisab kaardi k�tte
//		if (c == null)
//			throw new NullPointerException("Ei saa t�hja kaarti lisada.");
		hand.add(c);
	}

	public void removeCard(Card c) {
		hand.remove(c);
	}
	
	public ArrayList<Card> getCards() {
		return hand;
	}
	
	public String toString() {
		return hand.toString();
	}
}
