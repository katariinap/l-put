package loputoo;

public class Card {

	public final static int SPADES = 0; // Kood mastide jaoks + jokker
	public final static int HEARTS = 1;
	public final static int DIAMONDS = 2;
	public final static int CLUBS = 3;
	public final static int JOKER = 4;

	public final static int JACK = 11;
	public final static int QUEEN = 12;
	public final static int KING = 13;
	public final static int ACE = 1;

	private final int suit; // Kaardi mast

	private final int value; // Kaardi v��rtus
	
	public Card() { // Teeb jokkeri
		suit = JOKER;
		value = 1;
	}

	public Card(int theValue, int theSuit) {
		if (theSuit != SPADES && theSuit != HEARTS && theSuit != DIAMONDS && theSuit != CLUBS && theSuit != JOKER)
			throw new IllegalArgumentException("Illegaalne mast."); 																																		// pole																	// range-is
		if (theSuit != JOKER && (theValue < 1 || theValue > 15)) 																	
			throw new IllegalArgumentException("Illegaalne v��rtus.");
		value = theValue;
		suit = theSuit;
	}

	public int getSuit() {
		return suit;
	}

	public int getValue() {
		return value;
	}
	
	public int getStrength() {
		if (value == ACE) return 14;
		return value;
	}

	public String getSuitAsString() {
		switch (suit) {
		case SPADES:
			return "Poti";
		case HEARTS:
			return "�rtu";
		case DIAMONDS:
			return "Ruutu";
		case CLUBS:
			return "Risti";
		default:
			return "Jokker";

		}
	}

	public String getValueAsString() {
		if (suit == JOKER)
			return "" + value;
		else {
			switch (value) {
			case 2:
				return "2";
			case 3:
				return "3";
			case 4:
				return "4";
			case 5:
				return "5";
			case 6:
				return "6";
			case 7:
				return "7";
			case 8:
				return "8";
			case 9:
				return "9";
			case 10:
				return "10";
			case JACK:
				return "Poiss";
			case QUEEN:
				return "Emand";
			case KING:
				return "Kuningas";
			case ACE:
				return "�ss";
			default:
				return "!TUNDMATU KAART!";
			}
		}
	}

	public boolean strongerThan(Card card) {
		if (suit == JOKER) return true;
		
		if (suit == card.suit) {
			return (getStrength() > card.getStrength());
		} else {
			if (suit == HEARTS && card.suit != HEARTS) return true;
		}
		
		return false;
		
		
		// this.strongerThan(that);
		// TRUE, kui this on tugevam kaart kui that
		// FALSE, kui that on tugevam kui this
		
		// kaart1.strongerThan(kaart2)
		// TRUE, kui kaart1 on tugevam kui kaart2
	}
	
	public String toString() {
		return (getSuitAsString() + getValueAsString());
	}

}
