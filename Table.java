package loputoo;

import java.util.ArrayList;

public class Table {
	
	ArrayList<PlayedCard> playedCards; 
	
	public Table() {
		playedCards = new ArrayList<>();
	}
	
	public void playCard(Player player, Card card) {
		
		PlayedCard playedCard = new PlayedCard(player, card);
		playedCards.add(playedCard);
		player.hand.removeCard(card);
		
	}
	
	public Player findWinner() {
		PlayedCard startingCard = playedCards.get(0);
		PlayedCard strongestCard = startingCard;
		
		for (PlayedCard playedCard : playedCards) {
			Card card = playedCard.card;
			if (card.strongerThan(strongestCard.card)) {
				strongestCard = playedCard;
			}
		}
		return strongestCard.player;
	}
	
	public void printTable() {
		System.out.println("- M�NGULAUD ON J�RGMINE: -");
		for (PlayedCard playedCard : playedCards) {
			System.out.println("*" + playedCard.player.name + ": " + playedCard.card);
		}
	}

}
