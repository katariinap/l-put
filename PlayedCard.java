package loputoo;

public class PlayedCard {
	Player player;
	Card card;
	
	public PlayedCard(Player player, Card card) {
		this.card = card;
		this.player = player;	
	}
}
