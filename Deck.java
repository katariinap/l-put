package loputoo;

public class Deck {

	private Card[] deck; // Array kaartidega, 52 + 2 koos jokkeritega
	private int cardsUsed; // Mitu kaarti on pakist jagatud

	public Deck() { // Tavaline pakk 52 kaardiga
		this(false); // Konstruktori kutsumiseks
	}

	public Deck(boolean includeJokers) { // Jokkerite lisamiseks
		if (includeJokers) 
			deck = new Card[54];
		else
			deck = new Card[52];
		int cardCt = 0;
		for (int suit = 0; suit <= 3; suit++) {
			for (int value = 1; value <= 13; value++) {
				deck[cardCt] = new Card(value, suit);
				cardCt++;
			}
		}

		if (includeJokers) {
			deck[52] = new Card(1, Card.JOKER);
			deck[53] = new Card(2, Card.JOKER);
		}
		cardsUsed = 0;

	}

	public void shuffle() { //Kaartide tagasi panemine ja segamine
		for (int i = deck.length-1; i > 0; i--) {
			int rand = (int)(Math.random()*(i+1));
			Card temp = deck[i];
			deck[i] = deck[rand];
			deck[rand] = temp;
		}
		cardsUsed = 0;		
	}

	public int cardsLeft() { // Kui kaarte m�ngitakse, siis arv pakis v�heneb
		return deck.length - cardsUsed;
	}

	public Card dealCard() { // Eemaldab kaardi pakist
		if (cardsUsed == deck.length)
			throw new IllegalStateException("�htegi kaarti pole pakki j��nud.");
		cardsUsed++;
		return deck[cardsUsed - 1];
	}

	public boolean hasJokers() {
		return (deck.length == 54);
	}
}