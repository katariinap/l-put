package loputoo;

public class Player {
	String name;
	Hand hand;
	int points;
	int currentBet;
	int wins = 0;	
	
	public Player(String name) {
		this.name = name;
	}
		
	@Override
	public String toString() {
		return this.name;
	}
}
